package br.com.skylane.leakfinder;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

public class HelloAndroidActivity extends Activity {
	
	private boolean stop = false;
	private SpeedometerGauge s;
	
    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        s = (SpeedometerGauge) findViewById(R.id.gauge);
        // Add label converter
        s.setLabelConverter(new SpeedometerGauge.LabelConverter() {
            @Override
            public String getLabelFor(double progress, double maxProgress) {
                return String.valueOf((int) Math.round(progress));
            }
        });
        s.setMaxSpeed(200);
        //s.setSpeed(50, true);
        s.setUnitsText("ppm");
        
        startaBluetooth();
        
    }
    
    private void startaBluetooth() {
    	new Thread(new Runnable() {

			@Override
			public void run() {
				BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();

		        if (bluetoothAdapter == null) {
		            Toast.makeText(getApplicationContext(),"Device doesnt Support Bluetooth",Toast.LENGTH_SHORT).show();
		        }
		        
		        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();

		        if(bondedDevices.isEmpty()) {

		             Toast.makeText(getApplicationContext(),"Please Pair the Device first",Toast.LENGTH_SHORT).show();

		        } else {
		        	BluetoothDevice device = null;
		        	
					for (BluetoothDevice dev : bondedDevices) {

						if (dev.getName().equals("HC-05")) // Replace with
															// iterator.getName() if
															// comparing Device names.

						{

							device = dev; // device is an object of type
												// BluetoothDevice
							
							break;

						}
					}
					
					BluetoothSocket socket = null;
					
					try {
					
						try{
							//socket = device.createInsecureRfcommSocketToServiceRecord(UUID.randomUUID());
							
							try {
								Class<?> clazz = device.getClass();
								Class<?>[] paramTypes = new Class<?>[] { Integer.TYPE };
								Method m = clazz.getMethod("createRfcommSocket", paramTypes);
								Object[] params = new Object[] { Integer.valueOf(1) };
								socket = (BluetoothSocket) m.invoke(device, params);
																
							} catch (Exception e) {
								Log.d("LEAK", "Erro ao chamar o método", e);
							}
							
							Thread.sleep(500);
							
							if(socket != null)
								socket.connect();
							
							BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
							
							while (!stop) {
								
								//byte[] pacote = new byte[10];
								//int len = is.read(pacote);
								
								final String info = is.readLine();
								Log.d("LEAK", "*** " + info);
								
								runOnUiThread(new Runnable() {
									
									@Override
									public void run() {
										double value = Double.valueOf(info) / 10;
										s.setSpeed(value, 100, 100);
									}
								});
							}
						
						} finally {
							if (socket != null)
								socket.close();
						}
						
					} catch(Exception e) {
						Log.e("LEAK", "Erro de IO", e);
					} 
				}
				
			}
    		
    	}).start();
    	
		
	}

	/**
	 * Método que faz a leitura de um stream usando um buffer e retorna um byte[]
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static byte[] fazLeituraStreamEmByteArray(InputStream is) throws IOException{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[4096];
        int lidos;
        while ((lidos = is.read(buf)) > 0){
            baos.write(buf, 0, lidos);
        }
        baos.flush();
        byte[] bytes = baos.toByteArray();
        baos.close();
        return bytes;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(br.com.skylane.leakfinder.R.menu.main, menu);
	return true;
    }

    
    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	super.onDestroy();
    	
    	this.stop = true;
    }
}

