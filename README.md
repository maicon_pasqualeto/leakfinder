# Grupo Sem Furo

Nome do projeto: **leakfinder**

Desafio: **A**

Participantes:

- Maicon Dante Pasqualeto
- Emne Vanessa
- Percival Adriano
- Antonio Marcos
- Gaspar Kuhnen



# README #

Backend restfull para apoiar o aplicativo progressive web que serve como ferramenta para qualquer pessoa poder avisar sobre vazamentos de água e esgoto visívies.

### Licenca ###
Código fonte de licenca aberta.


### Versão ###

* versão 1.0-SNAPSHOT

### How do I get set up? ###

* Baixar e instalar Java JDK versão 8.x
	http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html
* Baixar e instalar Maven 3
	https://maven.apache.org/download.cgi